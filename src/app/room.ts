// import { Permission } from "../permission/Permission";

export class User {
    userId: number;
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    userType: number;
    phoneNumber: string;
    active: boolean;
    token: string;
    lastSeen: string;
    uuid: string;
    avatar: string;
    // permissions: Permission;
    parentId: number;
}

export class Room {
    displayName: string;
    email: string;
    type: string;
    phoneNumber: string;
    status: boolean;
    uuid: string;
    avatar: string;
    isPrivate: boolean;
}