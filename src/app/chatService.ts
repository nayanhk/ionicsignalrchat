import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';

import { Room } from './room';
import { Message } from './message';
// import { AuthenticationService } from '../app/authentication/authentication.service';


@Injectable()
export class ChatService {
    messageReceived = new EventEmitter<Message>();
    connectionEstablished = new EventEmitter<Boolean>();

    private connectionIsEstablished = false;
    private _hubConnection: HubConnection;
    // roomOpened = new Subject<Room>();
    // roomClosed = new Subject<Room>();

    constructor(
        // private readonly authenticationService: AuthenticationService
    ) {
        this.createConnection();
        this.registerOnServerEvents();
        this.startConnection();
    }

    sendMessage(message) {
        console.log('sending message here...', message);

        this._hubConnection.invoke('SendMessage', message);
    }


    createConnection() {
        this._hubConnection = new HubConnectionBuilder()
            .withUrl('https://washit.sanmolappspilot.com/chathub?uuid=32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e'
                // this.authenticationService.currentUserValue.uuid
            )
            .withAutomaticReconnect()
            .build();

    }

    startConnection() {
        this._hubConnection
            .start()
            .then(() => {
                this.connectionIsEstablished = true;
                console.log('Hub connection started');
                // this.joinChat()

                this.connectionEstablished.emit(true);
            })
            .catch(err => {
                console.log('Error while establishing connection, retrying...');
                setTimeout(function () { this.startConnection(); }, 5000);
            });
    }
    joinChat() {
        var currentuuid = '32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e'
        var detailerId = '187a08cf-2d57-4142-adba-2fae567c9690'
        this._hubConnection.invoke('Join', currentuuid, detailerId);
        console.log('join chat started');

    }

    // registerOnServerEvents() {
    //     this._hubConnection.on('JoinedSupportChat', (data: any) => {
    //         console.log('joined...', data);
    //         // if (!data.avatar)
    //         //     data.avatar = 'assets/img/user-profile.png';
    //         // this.guestUser.next(data);
    //     });
    //     // this._hubConnection.on('MessageReceived', (data: any) => {
    //     //     this.messageReceived.emit(data);
    //     //     console.log('incoming message...', data);
    //     // });

    // }
    private registerOnServerEvents(): void {


        this._hubConnection.on('JoinedSupportChat', (data: Room) => {

            if (!data.avatar)
                data.avatar = 'assets/img/user-profile.png';

            // this.guestUser.next(data);
        });

        this._hubConnection.on('LoadActiveRooms', (data: Room[]) => {

            // data.forEach((user) => {
            //     if (!user.avatar)
            //         user.avatar = 'assets/img/user-profile.png';
            // });

            // this.carUsers.next(data.filter(x => x.type == "CarUser" || x.type == "GuestUser"));
            // this.carDetailers.next(data.filter(x => x.type == "CarDetailer"));
        });

        this._hubConnection.on('LoadMessagesHistory', (data: Message[]) => {
            // var lastdate = '';
            console.log('message history ', data);


        });

        this._hubConnection.on('MessageReceived', (data: any) => {

            // data.timestamp = Methods.customDateFormat(new Date(parseInt(data.sentTimeStamp)), 'hh:mm a');

            console.log('incoming msessage', data);

            // this.message.next(data);
        });

        this._hubConnection.on('RoomClosed', (data: any) => {

            // this.roomClosed.next(data);

        });

        this._hubConnection.on('RoomOpened', (data: any) => {

            // this.roomOpened.next(data);

        });

    }
    getHistory() {

    }
    getMessagesHistory(fromId: '32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e', toId: '187a08cf-2d57-4142-adba-2fae567c9690', isPrivate: 2) {

        this._hubConnection.invoke('GetMessagesHistory', fromId, toId, isPrivate);
    }
}    