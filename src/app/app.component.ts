import { Component, NgZone } from '@angular/core';


import { ChatService } from '../app/chatService';

import { HomePage } from '../pages/home/home';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  title = 'ClientApp';
  txtMessage: string = '';
  uniqueID: string = new Date().getTime().toString();
  messages = new Array<Message>();
  message: Message;

  constructor(
    private chatService: ChatService,
    private _ngZone: NgZone

  ) {
    this.subscribeToEvents();
    // this.chatService.getHistory();
    var unixtimestamp = (new Date()).getTime() / 1000;
    console.log('time stamp', unixtimestamp);



  }
  generateUUID() {
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now() * 1000)) || 0; //Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16;//random number between 0 and 16
      if (d > 0) {//Use timestamp until depleted
        r = (d + r) % 16 | 0;
        d = Math.floor(d / 16);
      } else {//Use microseconds since page-load if supported
        r = (d2 + r) % 16 | 0;
        d2 = Math.floor(d2 / 16);
      }
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
  getUTCTime(data) {
    return Date.UTC(
      data.getUTCFullYear(),
      data.getUTCMonth(),
      data.getUTCDate(),
      data.getUTCHours(),
      data.getUTCMinutes(),
      data.getUTCSeconds()
    );
  }
  // msg.content = this.message.content;
  // msg.sentTimeStamp = Methods.getUTCTime(new Date()).toString();
  // msg.date = Methods.customDateFormat(new Date(parseInt(msg.sentTimeStamp)), 'EEEE, MMMM dd yyyy');
  // msg.timestamp = Methods.customDateFormat(new Date(parseInt(msg.sentTimeStamp)), 'hh:mm a');
  // msg.messageId = 0;
  // msg.avatar = environment.webApiUrl + '' + this.authenticationService.currentUserValue.profilePicture;
  // msg.uUID = Methods.generateUUID();
  sendMessage() {
    var msgObj = {
      uUID: this.generateUUID(),
      fromId: '32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e',
      toId: '187a08cf-2d57-4142-adba-2fae567c9690',
      content: 'hey mayur hows going on?',
      messageType: 2,
      sentTimeStamp: this.getUTCTime(new Date()).toString(),
    }
    this.chatService.sendMessage(msgObj);

  }
  subscribeToEvents() {

    this.chatService.messageReceived.subscribe((message: Message) => {
      this._ngZone.run(() => {
        if (message.id !== this.uniqueID) {
          // message.type = "received";
          this.messages.push(message);
        }
      });
    });
  }


}


