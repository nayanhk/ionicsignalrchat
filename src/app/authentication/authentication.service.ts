// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { BehaviorSubject, Observable, Subject } from 'rxjs';
// import { map } from 'rxjs/operators';


// @Injectable()
// export class AuthenticationService {
//     public currentUserSubject: BehaviorSubject<User>;
//     public currentUser: Subject<User>;

//     constructor(private http: HttpClient) {
//         this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
//         this.currentUser = this.currentUserSubject;
//     }
//     private httpOptions = {
//         headers: new HttpHeaders({
//             'Content-Type': 'application/json; charset=utf8',
//             'Access-Control-Allow-Origin': '*'
//         })
//     };
//     public get currentUserValue(): User {
//         return this.currentUserSubject.value;

//     }

//     login(email: string, password: string) {
//         return this.http.post<any>(`${environment.webApiUrl}Account/AuthenticateUser`, { email, password }, this.httpOptions)
//             .pipe(map(data => {


//                 // login successful if there's a jwt token in the response
//                 if (data.isSuccess && data.user && data.user.token) {

//                     data.user.permissions.menus.forEach((menu) => {

//                         menu.icon = MenuIcons.find(x => x.alias == menu.alias)?.icon;
//                     });
//                     // store user details and jwt token in local storage to keep user logged in between page refreshes
//                     localStorage.setItem('currentUser', JSON.stringify(data.user));
//                     this.currentUserSubject.next(data.user);
//                     this.currentUser.next(data.user);
//                 }

//                 return data;
//             }));
//     }

//     logout() {
//         // remove user from local storage to log user out
//         localStorage.removeItem('currentUser');
//         this.currentUserSubject.next(null);
//     }

//     public getById(userid: number): Observable<CustomerResponse> {
//         return this.http.get<CustomerResponse>(`${environment.webApiUrl}Account/GetUserById?userId=${userid}`, this.httpOptions);
//     }

//     public save(user: Customer, address: UserAddress): Observable<any> {
//         return this.http.post<any>(`${environment.webApiUrl}/Customer/CreateOrUpdateCustomer`,
//             { user: user, address: address }, this.httpOptions);
//     }

//     uploadFile(uploadedFiles: any[]) {
//         let file: File = null;
//         let formData: FormData = new FormData();
//         if (uploadedFiles != null && uploadedFiles.length > 0) {
//             file = uploadedFiles[0];
//         }
//         if (file != null) {
//             formData.append("img", file, file.name);
//             // formData.append("fileName", file.name);
//         } else {
//             formData.append("img", null);
//         }
//         return this.http.post<ImageResponse>(`${environment.webApiUrl}File/ProfileImage`, formData);
//     }

//     uploadFileUsingPath(uploadedFiles: any[],path:string) {
//       let file: File = null;
//       let formData: FormData = new FormData();
//       if (uploadedFiles != null && uploadedFiles.length > 0) {
//           file = uploadedFiles[0];
//       }
//       if (file != null) {
//           formData.append("img", file, file.name);
//           formData.append("path",path);

//           // formData.append("fileName", file.name);
//       } else {
//           formData.append("img", null);
//       }
//       return this.http.post<ImageResponse>(`${environment.webApiUrl}File/UploadImageUsingPath`, formData);
//   }


//     public changePassword(user: Customer): Observable<any> {
//         user.userId = this.currentUserValue.userId;
//         return this.http.post<any>(`${environment.webApiUrl}User/ChangePassword`,
//             user, this.httpOptions);
//     }



// }
