export class Message 
{
    messageId:number;
    content:string;
    toId:string;
    toName:string;
    fromId:string;
    fromName:string;
    sentTimeStamp:string;
    readTimeStamp:string;
    isMine:boolean;
    uUID:string;
    timestamp:string;
    date:string;
}
export class MessageHistoryResponse {
    isSuccess: boolean;
    messages: Message[];
}