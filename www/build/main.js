webpackJsonp([0],{

/***/ 126:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 126;

/***/ }),

/***/ 168:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 168;

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__microsoft_signalr__ = __webpack_require__(307);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { AuthenticationService } from '../app/authentication/authentication.service';
var ChatService = /** @class */ (function () {
    // roomOpened = new Subject<Room>();
    // roomClosed = new Subject<Room>();
    function ChatService() {
        this.messageReceived = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.connectionEstablished = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.connectionIsEstablished = false;
        this.createConnection();
        this.registerOnServerEvents();
        this.startConnection();
    }
    ChatService.prototype.sendMessage = function (message) {
        console.log('sending message here...', message);
        this._hubConnection.invoke('SendMessage', message);
    };
    ChatService.prototype.createConnection = function () {
        this._hubConnection = new __WEBPACK_IMPORTED_MODULE_1__microsoft_signalr__["a" /* HubConnectionBuilder */]()
            .withUrl('https://washit.sanmolappspilot.com/chathub?uuid=32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e'
        // this.authenticationService.currentUserValue.uuid
        )
            .withAutomaticReconnect()
            .build();
    };
    ChatService.prototype.startConnection = function () {
        var _this = this;
        this._hubConnection
            .start()
            .then(function () {
            _this.connectionIsEstablished = true;
            console.log('Hub connection started');
            // this.joinChat()
            _this.connectionEstablished.emit(true);
        })
            .catch(function (err) {
            console.log('Error while establishing connection, retrying...');
            setTimeout(function () { this.startConnection(); }, 5000);
        });
    };
    ChatService.prototype.joinChat = function () {
        var currentuuid = '32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e';
        var detailerId = '187a08cf-2d57-4142-adba-2fae567c9690';
        this._hubConnection.invoke('Join', currentuuid, detailerId);
        console.log('join chat started');
    };
    // registerOnServerEvents() {
    //     this._hubConnection.on('JoinedSupportChat', (data: any) => {
    //         console.log('joined...', data);
    //         // if (!data.avatar)
    //         //     data.avatar = 'assets/img/user-profile.png';
    //         // this.guestUser.next(data);
    //     });
    //     // this._hubConnection.on('MessageReceived', (data: any) => {
    //     //     this.messageReceived.emit(data);
    //     //     console.log('incoming message...', data);
    //     // });
    // }
    ChatService.prototype.registerOnServerEvents = function () {
        this._hubConnection.on('JoinedSupportChat', function (data) {
            if (!data.avatar)
                data.avatar = 'assets/img/user-profile.png';
            // this.guestUser.next(data);
        });
        this._hubConnection.on('LoadActiveRooms', function (data) {
            // data.forEach((user) => {
            //     if (!user.avatar)
            //         user.avatar = 'assets/img/user-profile.png';
            // });
            // this.carUsers.next(data.filter(x => x.type == "CarUser" || x.type == "GuestUser"));
            // this.carDetailers.next(data.filter(x => x.type == "CarDetailer"));
        });
        this._hubConnection.on('LoadMessagesHistory', function (data) {
            // var lastdate = '';
            console.log('message history ', data);
        });
        this._hubConnection.on('MessageReceived', function (data) {
            // data.timestamp = Methods.customDateFormat(new Date(parseInt(data.sentTimeStamp)), 'hh:mm a');
            console.log('incoming msessage', data);
            // this.message.next(data);
        });
        this._hubConnection.on('RoomClosed', function (data) {
            // this.roomClosed.next(data);
        });
        this._hubConnection.on('RoomOpened', function (data) {
            // this.roomOpened.next(data);
        });
    };
    ChatService.prototype.getHistory = function () {
    };
    ChatService.prototype.getMessagesHistory = function (fromId, toId, isPrivate) {
        this._hubConnection.invoke('GetMessagesHistory', fromId, toId, isPrivate);
    };
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ChatService);
    return ChatService;
}());

//# sourceMappingURL=chatService.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_socket_io_client__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.chats = [];
        if (this.myUserId == null) {
            this.myUserId = Date.now().toString();
        }
        this.socket = __WEBPACK_IMPORTED_MODULE_2_socket_io_client__('http://localhost:3000');
        this.Receive();
    }
    HomePage.prototype.send = function (msg) {
        if (msg != '') {
            var saltedMsg = this.myUserId + "#" + msg;
            this.socket.emit('message', saltedMsg);
            console.log('data sending');
        }
        this.chat_input = '';
    };
    HomePage.prototype.Receive = function () {
        var _this = this;
        this.socket.on('message', function (msg) {
            var saletdMsgArr = msg.split('#');
            var item = {};
            if (saletdMsgArr[0] == _this.myUserId) {
                item = { "styleClass": "chat-message right", "msgStr": saletdMsgArr[1] };
            }
            else {
                item = { "styleClass": "chat-message left", "msgStr": saletdMsgArr[1] };
            }
            _this.chats.push(item);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/sanmol/Downloads/simple_chat_applcation-master/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n       <ion-icon md="md-chatbubbles"></ion-icon> Chat Room\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngFor="let message of chats">\n    <div [ngClass]="message.styleClass">\n	    <div class="message-detail">\n        {{message.msgStr}}\n	    </div>\n    </div>\n  </div>\n\n</ion-content>\n\n<ion-footer>\n  <ion-item>\n    <ion-input type="text" [(ngModel)]="chat_input" placeholder="Enter message"></ion-input>\n    <ion-icon name="md-send" (click)="send(chat_input)" item-right></ion-icon>\n  </ion-item>\n</ion-footer>'/*ion-inline-end:"/Users/sanmol/Downloads/simple_chat_applcation-master/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(253);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__chatService__ = __webpack_require__(210);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__chatService__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_chatService__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_home_home__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MyApp = /** @class */ (function () {
    function MyApp(chatService, _ngZone) {
        this.chatService = chatService;
        this._ngZone = _ngZone;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__pages_home_home__["a" /* HomePage */];
        this.title = 'ClientApp';
        this.txtMessage = '';
        this.uniqueID = new Date().getTime().toString();
        this.messages = new Array();
        this.subscribeToEvents();
        // this.chatService.getHistory();
        var unixtimestamp = (new Date()).getTime() / 1000;
        console.log('time stamp', unixtimestamp);
    }
    MyApp.prototype.generateUUID = function () {
        var d = new Date().getTime(); //Timestamp
        var d2 = (performance && performance.now && (performance.now() * 1000)) || 0; //Time in microseconds since page-load or 0 if unsupported
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16; //random number between 0 and 16
            if (d > 0) {
                r = (d + r) % 16 | 0;
                d = Math.floor(d / 16);
            }
            else {
                r = (d2 + r) % 16 | 0;
                d2 = Math.floor(d2 / 16);
            }
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    };
    MyApp.prototype.getUTCTime = function (data) {
        return Date.UTC(data.getUTCFullYear(), data.getUTCMonth(), data.getUTCDate(), data.getUTCHours(), data.getUTCMinutes(), data.getUTCSeconds());
    };
    // msg.content = this.message.content;
    // msg.sentTimeStamp = Methods.getUTCTime(new Date()).toString();
    // msg.date = Methods.customDateFormat(new Date(parseInt(msg.sentTimeStamp)), 'EEEE, MMMM dd yyyy');
    // msg.timestamp = Methods.customDateFormat(new Date(parseInt(msg.sentTimeStamp)), 'hh:mm a');
    // msg.messageId = 0;
    // msg.avatar = environment.webApiUrl + '' + this.authenticationService.currentUserValue.profilePicture;
    // msg.uUID = Methods.generateUUID();
    MyApp.prototype.sendMessage = function () {
        var msgObj = {
            uUID: this.generateUUID(),
            fromId: '32b4dbb9-ec5c-40d9-9bcb-b1579e93b76e',
            toId: '187a08cf-2d57-4142-adba-2fae567c9690',
            content: 'hey mayur hows going on?',
            messageType: 2,
            sentTimeStamp: this.getUTCTime(new Date()).toString(),
        };
        this.chatService.sendMessage(msgObj);
    };
    MyApp.prototype.subscribeToEvents = function () {
        var _this = this;
        this.chatService.messageReceived.subscribe(function (message) {
            _this._ngZone.run(function () {
                if (message.id !== _this.uniqueID) {
                    // message.type = "received";
                    _this.messages.push(message);
                }
            });
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/sanmol/Downloads/simple_chat_applcation-master/src/app/app.html"*/'<div class="container">\n  <h3 class=" text-center chat_header">Chat Application</h3>\n  <div class="messaging">\n    <div class="inbox_msg">\n      <div class="mesgs">\n        <div class="msg_history">\n          <div *ngFor="let msg of messages">\n            <div class="incoming_msg" *ngIf="msg.type == \'received\'">\n              <div class="incoming_msg_img"></div>\n              <div class="received_msg">\n                <div class="received_withd_msg">\n                  <p>\n                    {{msg.message}}\n                  </p>\n                  <span class="time_date"> {{msg.date | date:\'medium\'}} </span>\n                </div>\n              </div>\n            </div>\n            <div class="outgoing_msg" *ngIf="msg.type == \'sent\'">\n              <div class="sent_msg">\n                <p>\n                  {{msg.message}}\n                </p>\n                <span class="time_date"> {{msg.date | date:\'medium\'}}</span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class="type_msg">\n          <div class="input_msg_write">\n            <input\n              type="text"\n              class="write_msg"\n              [value]="txtMessage"\n              (input)="txtMessage=$event.target.value"\n              (keydown.enter)="sendMessage()"\n              placeholder="Type a message"\n            />\n            <button class="msg_send_btn" type="button" (click)="sendMessage()">\n              <i class="fa fa-paper-plane-o" aria-hidden="true">send</i>\n            </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n'/*ion-inline-end:"/Users/sanmol/Downloads/simple_chat_applcation-master/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_chatService__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 337:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[229]);
//# sourceMappingURL=main.js.map